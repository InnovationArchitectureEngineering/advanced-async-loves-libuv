/**
 * Game root wrapper & runner
 */

"use strict";

// Config
const Env = require("./Config/TstEnv");

// Game Engine
const StockGeneratorClass = require("./Engine/StockGenerator");
const StockGenerator = new StockGeneratorClass;
const ToolBoxClass = require("./Engine/ToolBox");
const ToolBox = new ToolBoxClass;

// Model
const UserModel = require("./Model/User");
const TileModel = require("./Model/Tile");

// Content
const Msg = require("./Content/Msg/EN");

// Root wrapper
const PlayGameInConsole = async () => {

	try {
		// Data containers & helpers vars definition
		let player = [];
		let stock = [];
		let board = [];
		
		let tile = {};
		let tempObject = {};

		let playersTurnIndexInt = null;
		let winState = false;

		// Instantiate Tile entity &
		// mapping entity's properties with Env Config properties
		tile = new TileModel();
		tile.Style.Start = Env.Tile.Style.Start;
		tile.Style.Delimiter = Env.Tile.Style.Delimiter;
		tile.Style.End = Env.Tile.Style.End;
		tile.Square.Up.Key = Env.Tile.Square.Up.Key;
		tile.Square.Down.Key = Env.Tile.Square.Down.Key;

		// Initialize a new shuffled stock of Tiles
		await StockGenerator.generate();
		stock = await ToolBox.shuffleArray(StockGenerator.getStockBucket());

		// Each player draws 7 random tiles from the stock, to hold in his/her deck
		for (let i = 0; i < Env.User.length; i++) {
			
			// Instantiate Players' entities &
			// mapping entity's properties with Env Config properties
			player[i] = new UserModel();
			player[i].Id = parseInt(Env.User[i].Id);
			player[i].Status = parseInt(Env.User[i].Status);
			player[i].Name = Env.User[i].Name;
			
			tempObject = await ToolBox.drawSevenRandomTiles(stock);
			
			// Initialize each Player's deck
			player[i].Deck = tempObject.deck;

			// Update stock of Tiles
			stock = tempObject.stock;

			// Security line | return to default
			tempObject = {};
		}

		// Draw a random first tile to start the game
		tempObject = await ToolBox.drawOneRandomTile(stock);
		
		// Update stock of Tiles
		stock = tempObject.stock;

		// Initialize board of Tiles
		await board.push(tempObject.tile);

		// Security line | return to default
		tempObject = {};

		// Message | Game has started | Intro
		await ToolBox.log(
			await ToolBox.consoleBeautify(
				await ToolBox.boardStatusStr(
					Msg.Status.Intro,
					board,
					tile.Style.Start,
					tile.Style.Delimiter,
					tile.Style.End,
					Env.Content.Msg.Nbsp
		)));

		// Decide who plays first
		playersTurnIndexInt = await ToolBox.whoPlaysFirst(player);

		// Game starts | Game event loop
		while (winState === false) {

			tempObject = await ToolBox.playerMakesDecision(
				await ToolBox.playerExaminesPossibleOptions(player[playersTurnIndexInt].Deck, board),
				player[playersTurnIndexInt].Deck,
				stock,
				board,
				tile.Square.Up.Key,
				tile.Square.Down.Key
			);

			// Updates are applied where & if needed
			player[playersTurnIndexInt].Deck = tempObject.deck;
			stock = tempObject.stock;
			board = tempObject.board;

			// Message | Game is active | Player status
				await ToolBox.log(
					await ToolBox.consoleBeautify(
						await ToolBox.playerStatusStr(
							player[playersTurnIndexInt].Name,
							Msg.Player,
							tempObject.tileDrawStatus,
							tempObject.drawTile,
							tempObject.playTile,
							tempObject.connectToBoardTile,
							tempObject.tileReverseStatus,
							tile.Style.Start,
							tile.Style.Delimiter,
							tile.Style.End,
							Env.Content.Msg.Nbsp,
							tile.Square.Up.Key,
							tile.Square.Down.Key
				)));

			// Security line | return to default
			tempObject = {};

			// Message | Game is active | Board status
			await ToolBox.log(
				await ToolBox.consoleBeautify(
					await ToolBox.boardStatusStr(
						Msg.Status.Board,
						board,
						tile.Style.Start,
						tile.Style.Delimiter,
						tile.Style.End,
						Env.Content.Msg.Nbsp
			)));

			if (player[playersTurnIndexInt].Deck.length === 0) {

				winState = true;

				// Message | Game is finished | Win status
				await ToolBox.log(
					await ToolBox.consoleBeautify(
						await ToolBox.winStatusStr(
						Msg.Status.Winner,
						player[playersTurnIndexInt].Name,
						Env.Content.Msg.Nbsp
				)));
			}

			if (winState === false) {
				playersTurnIndexInt = await ToolBox.whoPlaysNext(player, playersTurnIndexInt);
			}
		}

		return;
	}

	catch(error) { console.error(error); }
};

// Runner
PlayGameInConsole();
