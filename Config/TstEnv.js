/**
 * Test environment configuration
 */

"use strict";

module.exports = {
	
	"User": [
		{
			"Id": "1",
			"Status": "1",
			"Name": "Alice"
		},
		{
			"Id": "2",
			"Status": "1",
			"Name": "Bob"
		}
	],
	
	"Tile": {

		"Style" : {
			"Start": "&#60;",
			"Delimiter": "&#58;",
			"End": "&#62;"
		},
		
		"Square": {
			"Up": {
				"Key": 0
			},
			"Down": {
				"Key": 1				
			}
		}
	},

	"Content": {
		"Msg": {
			"Nbsp": " "
		}
	}
};
