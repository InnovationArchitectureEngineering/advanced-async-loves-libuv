/**
 * Game messages content in English
 */

"use strict";

module.exports = {
	
	"Player": {
		"Plays": {
			"Start": "plays",
			"Where": "to connect to tile",
			"End": "on the board"
		},
		"Draws": "can&#39;t play&#44; drawing tile",
		"Reverse": "&#40;reversed&#41;"
	},

	"Status": {
		"Intro": "Game starting with first tile&#58;",
		"Board": "Board is now&#58;",
		"Winner": {
			"Start": "Player",
			"End": "has won&#33;"
		}
	}
};
