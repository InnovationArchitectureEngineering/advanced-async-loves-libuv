/**
 * Data model for the entity: User
 */

"use strict";

function User() {
	this.Id = null;
	this.Status = null;
	this.Name = "";
	this.Deck = [];
}

module.exports = User;
