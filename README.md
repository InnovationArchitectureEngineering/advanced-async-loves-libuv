```bash
/**
 * Advanced asynchronous programming concepts with love for https://libuv.org
 *
 * Features
 * - Extra randomized behaviour, to simulate human players behaviour and decisions
 * - Extra dynamic code to be easily refactored to: have more than 2 players,
 * - handle bigger stock of tiles and more than 2 tile-dimentions (can upgrade to 3D game)
 * - Applying Tile reversing when needed
 *
 * Dependencies (0)
 *
 * License GPL-3.0
 *
 * (c) https://www.linkedin.com/in/arissc/
 */
```

### Guide
- git clone this_repo
- npm start

### Sample Log
```bash
# Game starting with first tile: <0:3>
# Bob plays <0:0> to connect to tile <0:3>
# Board is now: <0:0> <0:3>
# Alice plays <3:5> to connect to tile <0:3>
# Board is now: <0:0> <0:3> <3:5>
# Bob plays <0:1> (reversed) to connect to tile <0:0>
# Board is now: <1:0> <0:0> <0:3> <3:5>
# Alice plays <1:1> to connect to tile <1:0>
# Board is now: <1:1> <1:0> <0:0> <0:3> <3:5>
# Bob plays <1:5> (reversed) to connect to tile <1:1>
# Board is now: <5:1> <1:1> <1:0> <0:0> <0:3> <3:5>
# Alice can't play, drawing tile <1:4>
# Board is now: <5:1> <1:1> <1:0> <0:0> <0:3> <3:5>
# Bob plays <2:5> to connect to tile <5:1>
# Board is now: <2:5> <5:1> <1:1> <1:0> <0:0> <0:3> <3:5>
# Alice plays <0:2> to connect to tile <2:5>
# Board is now: <0:2> <2:5> <5:1> <1:1> <1:0> <0:0> <0:3> <3:5>
# Bob plays <5:6> to connect to tile <3:5>
# Board is now: <0:2> <2:5> <5:1> <1:1> <1:0> <0:0> <0:3> <3:5> <5:6>
# Alice plays <3:6> (reversed) to connect to tile <5:6>
# Board is now: <0:2> <2:5> <5:1> <1:1> <1:0> <0:0> <0:3> <3:5> <5:6> <6:3>
# Bob plays <3:3> to connect to tile <6:3>
# Board is now: <0:2> <2:5> <5:1> <1:1> <1:0> <0:0> <0:3> <3:5> <5:6> <6:3> <3:3>
# Alice plays <0:6> (reversed) to connect to tile <0:2>
# Board is now: <6:0> <0:2> <2:5> <5:1> <1:1> <1:0> <0:0> <0:3> <3:5> <5:6> <6:3> <3:3>
# Bob can't play, drawing tile <1:3>
# Board is now: <6:0> <0:2> <2:5> <5:1> <1:1> <1:0> <0:0> <0:3> <3:5> <5:6> <6:3> <3:3>
# Alice plays <6:6> to connect to tile <6:0>
# Board is now: <6:6> <6:0> <0:2> <2:5> <5:1> <1:1> <1:0> <0:0> <0:3> <3:5> <5:6> <6:3> <3:3>
# Bob plays <1:3> (reversed) to connect to tile <3:3>
# Board is now: <6:6> <6:0> <0:2> <2:5> <5:1> <1:1> <1:0> <0:0> <0:3> <3:5> <5:6> <6:3> <3:3> <3:1>
# Alice plays <1:4> to connect to tile <3:1>
# Board is now: <6:6> <6:0> <0:2> <2:5> <5:1> <1:1> <1:0> <0:0> <0:3> <3:5> <5:6> <6:3> <3:3> <3:1> <1:4>
# Bob can't play, drawing tile <0:5>
# Board is now: <6:6> <6:0> <0:2> <2:5> <5:1> <1:1> <1:0> <0:0> <0:3> <3:5> <5:6> <6:3> <3:3> <3:1> <1:4>
# Alice plays <0:4> (reversed) to connect to tile <1:4>
# Board is now: <6:6> <6:0> <0:2> <2:5> <5:1> <1:1> <1:0> <0:0> <0:3> <3:5> <5:6> <6:3> <3:3> <3:1> <1:4> <4:0>
# Player Alice has won!
```
